<?php

namespace Tests\Feature\Console\Commands;

use App\Console\Commands\LoadCustomers;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LoadCustomersTest extends TestCase
{
    const TEMPLATE = [
        ['Вася', 'Пупкин', 'vasya.pupkin@mail.ru', '1980-02-29', '54.983334,73.366669', 'RUS'],
        ['Вася', 'Пупкин', 'vasya.pupkin@mail.ru', '1980-02-29', '54.983334,73.366669', 'RUS'], // duplicate email
        ['Вася', 'Пупкин', '1@1.1', '1980-02-29', '54.983334,73.366669', 'RUS'], // incorrect email
        ['Вася', 'Пупкин', 'vasya.pupkin1@mail.ru', '2022-02-01', '54.983334,73.366669', 'RUS'], // < 18 age ago
        ['Вася', 'Пупкин', 'vasya.pupkin2@mail.ru', '1880-02-29', '54.983334,73.366669', 'RUS'], // > 99 age ago
        ['Вася', 'Пупкин', 'vasya.pupkin3@mail.ru', '1980-02-29', '-700.983334,450.366669', 'RUS'], // unknown location
        ['Вася', 'Пупкин', 'vasya.pupkin4@mail.ru', '1980-02-29', '54.983334,73.366669', 'RRR'], // wrong country_code
    ];

    /**
     * Если нужно создать файл и вызывать комманду тестом
     *
     * @return void
     */
    public function test_load_customers()
    {
        $file = '/var/www/html/storage/app/raw_load_customers.csv';

        $fh = fopen($file, 'w+');
        foreach (static::TEMPLATE as $row) {
            fputcsv($fh, $row, ';');
        }
        fclose($fh);

        Artisan::call('load:customers', ['file' => $file]);
        $this->assertTrue(true);
    }

    /**
     * @return void
     */
    public function test_parse()
    {
        $errors = [];
        $insert = [];

        $handler = new LoadCustomers();
        foreach (static::TEMPLATE as $row) {
            $row[2] = str_replace('vasya.', '', $row[2]);
            $handler->parse($row, $insert, $errors);
        }

        $this->assertTrue(count($insert) == 2);
        $this->assertTrue(count($errors) == 5);
    }

    /**
     * @return void
     */
    public function test_is_valid_email()
    {
        $handler = new LoadCustomers();
        $this->assertTrue($handler->is_valid_email('juggernout@gmail.com'));
        $this->assertFalse($handler->is_valid_email('1@1.1'));
        $this->assertFalse($handler->is_valid_email('asd@ljndvnlds.com'));
    }

    /**
     * @return void
     */
    public function test_is_valid_age()
    {
        $handler = new LoadCustomers();
        $this->assertTrue($handler->is_valid_age('1980-02-29'));
        $this->assertFalse($handler->is_valid_age('2022-02-01'));
        $this->assertFalse($handler->is_valid_age('1880-02-29'));
    }

    /**
     * @return void
     */
    public function test_is_valid_location()
    {
        $handler = new LoadCustomers();
        $this->assertTrue($handler->is_valid_location('54.983334,73.366669'));
        $this->assertFalse($handler->is_valid_location('-700.983334,450.366669'));
    }

    /**
     * @return void
     */
    public function test_is_valid_country_code()
    {
        $handler = new LoadCustomers();
        $this->assertTrue($handler->is_valid_country_code('RUS'));
        $this->assertFalse($handler->is_valid_country_code('RRR'));
    }
}
