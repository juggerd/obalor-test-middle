<?php

namespace App\Console\Commands;

use App\Exports\LoadCustomersErrorsExport;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class LoadCustomers extends Command
{
    // Тягать по api эти данные, можно, но так быстрее
    const COUNTRY_CODES = ["AFG", "ALB", "DZA", "ASM", "AND", "AGO", "AIA", "ATA", "ATG", "ARG", "ARM", "ABW", "AUS",
        "AUT", "AZE", "BHS", "BHR", "BGD", "BRB", "BLR", "BEL", "BLZ", "BEN", "BMU", "BTN", "BOL", "BES", "BIH", "BWA",
        "BVT", "BRA", "IOT", "BRN", "BGR", "BFA", "BDI", "CPV", "KHM", "CMR", "CAN", "CYM", "CAF", "TCD", "CHL", "CHN",
        "CXR", "CCK", "COL", "COM", "COD", "COG", "COK", "CRI", "HRV", "CUB", "CUW", "CYP", "CZE", "CIV", "DNK", "DJI",
        "DMA", "DOM", "ECU", "EGY", "SLV", "GNQ", "ERI", "EST", "SWZ", "ETH", "FLK", "FRO", "FJI", "FIN", "FRA", "GUF",
        "PYF", "ATF", "GAB", "GMB", "GEO", "DEU", "GHA", "GIB", "GRC", "GRL", "GRD", "GLP", "GUM", "GTM", "GGY", "GIN",
        "GNB", "GUY", "HTI", "HMD", "VAT", "HND", "HKG", "HUN", "ISL", "IND", "IDN", "IRN", "IRQ", "IRL", "IMN", "ISR",
        "ITA", "JAM", "JPN", "JEY", "JOR", "KAZ", "KEN", "KIR", "PRK", "KOR", "KWT", "KGZ", "LAO", "LVA", "LBN", "LSO",
        "LBR", "LBY", "LIE", "LTU", "LUX", "MAC", "MDG", "MWI", "MYS", "MDV", "MLI", "MLT", "MHL", "MTQ", "MRT", "MUS",
        "MYT", "MEX", "FSM", "MDA", "MCO", "MNG", "MNE", "MSR", "MAR", "MOZ", "MMR", "NAM", "NRU", "NPL", "NLD", "NCL",
        "NZL", "NIC", "NER", "NGA", "NIU", "NFK", "MNP", "NOR", "OMN", "PAK", "PLW", "PSE", "PAN", "PNG", "PRY", "PER",
        "PHL", "PCN", "POL", "PRT", "PRI", "QAT", "MKD", "ROU", "RUS", "RWA", "REU", "BLM", "SHN", "KNA", "LCA", "MAF",
        "SPM", "VCT", "WSM", "SMR", "STP", "SAU", "SEN", "SRB", "SYC", "SLE", "SGP", "SXM", "SVK", "SVN", "SLB", "SOM",
        "ZAF", "SGS", "SSD", "ESP", "LKA", "SDN", "SUR", "SJM", "SWE", "CHE", "SYR", "TWN", "TJK", "TZA", "THA", "TLS",
        "TGO", "TKL", "TON", "TTO", "TUN", "TUR", "TKM", "TCA", "TUV", "UGA", "UKR", "ARE", "GBR", "UMI", "USA", "URY",
        "UZB", "VUT", "VEN", "VNM", "VGB", "VIR", "WLF", "ESH", "YEM", "ZMB", "ZWE", "ALA"];

    const INSERT_LIMIT = 500;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:customers {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load customers from CSV-file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = $this->argument('file');

        if (!file_exists($file)) {
            $this->alert('File not found!');
            return 1;
        }

        $errors = [];
        $insert = [];

        $fh = fopen($file, "r");
        while (($row = fgetcsv($fh, 0, ';')) !== false) {
            $this->parse($row, $insert, $errors);
        }
        fclose($fh);

        if (!empty($insert)) {
            $this->insert($insert);
            unset($insert);
        }

        if (!empty($errors)) {
            $this->save_report($errors);
            unset($errors);
        }

        return 0;
    }

    public function parse($row, &$insert, &$errors): void
    {
        $email = \Str::limit($row[2], 255);
        if (array_key_exists($email, $insert) || !$this->is_valid_email($row[2])) {
            $row['errors'] = 'email';
            $errors[] = $row;
            return;
        }
        if (!$this->is_valid_age($row[3])) {
            $row['errors'] = 'age';
            $errors[] = $row;
            return;
        }
        if (!$this->is_valid_country_code($row[5])) {
            $row['errors'] = 'country_code';
            $errors[] = $row;
            return;
        }
        $location = \Str::limit($row[4], 255);
        if (!$this->is_valid_location($location)) {
            $location = 'Unknown';
        }
        $now = Carbon::now();
        $insert[$email] = [
            'name' => \Str::limit($row[0], 255),
            'surname' => \Str::limit($row[1], 255),
            'email' => $email,
            'age' => Carbon::parse($row[3]),
            'location' => $location,
            'country_code' => $row[5],
            'created_at' => $now,
            'updated_at' => $now
        ];
    }

    /**
     * @param array $data
     * @return void
     */
    public function save_report(array $data): void
    {
        $xls_name = 'load_customers_errors_' . date('Ymd_His') . '.xls';
        Excel::store(new LoadCustomersErrorsExport($data), $xls_name);
    }

    /**
     * @param array $data
     * @return void
     */
    public function insert(array $data): void
    {
        foreach (collect($data)->chunk(static::INSERT_LIMIT) as $chunk) {
            \DB::table('customers')->insert($chunk->toArray());
        }
    }

    /**
     * @param string $email
     * @return bool
     */
    public function is_valid_email(string $email): bool
    {
        // unique:customers проверять по базе каждый email
        // такое себе, но если база огромная, то получать
        // и потом сравнивать тоже не весело
        $validator = \Validator::make(
            ['email' => $email],
            ['email' => 'required|unique:customers|email:rfc,dns']
        );
        return $validator->passes();
    }

    /**
     * @param string $age
     * @return bool
     */
    public function is_valid_age(string $age): bool
    {
        try {
            $diff = Carbon::parse($age)->diffInYears(Carbon::now());
            return $diff > 18 && $diff < 99;
        } catch (\InvalidDateException) {
            return false;
        }
    }

    /**
     * @param string $location
     * @return bool
     */
    public function is_valid_location(string $location): bool
    {
        $result = explode(',', $location);
        if (count($result) != 2) {
            return false;
        }
        $lat = (float)$result[0];
        $lon = (float)$result[1];
        return $lat >= -90 && $lat <= 90 && $lon >= -180 && $lon <= 180;
    }

    /**
     * @param string $country_code
     * @return bool
     */
    public function is_valid_country_code(string $country_code): bool
    {
        // Arr::has - evil
        return in_array($country_code, self::COUNTRY_CODES);
    }
}
