### Первичная установка по инструкции
https://laravel.com/docs/9.x#getting-started-on-macos

### После в папку с проектом вытянуть с моего репо
```
git clone https://bitbucket.org/juggerd/obalor-test-middle
```

### Доустановить
```
./vendor/bin/sail composer update
```

### Миграции
```
./vendor/bin/sail artisan migrate
```

### Тест
```
./vendor/bin/sail artisan test --testsuite=Feature --filter LoadCustomersTest --stop-on-failure
```

### Запуск
```
./vendor/bin/sail artisan load:customers /var/www/html/storage/app/raw_load_customers.csv
```

### Результирующие файлы в папке storage/app
load_customers_errors_20220317_165958.xls  
raw_load_customers.csv

### Основной код:
cmd:
https://bitbucket.org/juggerd/obalor-test-middle/src/master/app/Console/Commands/LoadCustomers.php

test:
https://bitbucket.org/juggerd/obalor-test-middle/src/master/tests/Feature/Console/Commands/LoadCustomersTest.php